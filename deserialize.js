import {DESERIALIZE_POS1_TABLE, DESERIALIZE_POS0_TABLE, SERIALIZE_POS0_TABLE_SIZE} from './utils.js'

export function deserialize(text) {
    return deserialize_text_to_list(text);
}

export function deserialize_text(text) {
    return deserialize_text_to_list(text);
}

function deserialize_text_to_list(text) {
    var out_list = [];
    var parse_data = { 'char_index': 0 };

    function get_char() {
        if (!has_char()) {
            return null;
        }
        var char_index = parse_data['char_index'];
        var char = text[char_index];
        parse_data['char_index'] = char_index + 1;
        return char;
    }

    function has_char() {
        return (parse_data['char_index'] < text.length);
    }

    var table0 = DESERIALIZE_POS0_TABLE;
    var table1 = DESERIALIZE_POS1_TABLE;

    function is_char_of_pos0(char) {
        return table0.hasOwnProperty(char);
    }

    function is_char_of_pos1(char) {
        return table1.hasOwnProperty(char);
    }

    function deserialize_one_char(char) {
        return parseInt(table0[char]);
    }

    function deserialize_two_chars(first_char, second_char) {
        return parseInt(table0[second_char]) + parseInt(table1[first_char]) * SERIALIZE_POS0_TABLE_SIZE;
    }

    function deserialize_chars(first_char, second_char) {
        if (second_char == null) {
            return deserialize_one_char(first_char);
        }
        return deserialize_two_chars(first_char, second_char);
    }

    while (has_char()) {
        let first_char = get_char();
        let second_char = null;
        if (is_char_of_pos1(first_char)) {
            if (!has_char()) {
            }
            second_char = get_char();
        }
        let value = deserialize_chars(first_char, second_char);
        out_list.push(value);
    }
    return out_list;
}
