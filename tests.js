import {check_serialization, generateArray} from './main.js';

let test = [10];
console.log('Test single number')
check_serialization(test);

let test6 = [178];
console.log('Test single number')
check_serialization(test6);

let test2 = generateArray(1 ,50, 300);
console.log('Test 50 random numbers')
check_serialization(test2);

let test3 = generateArray(1, 100, 300);
console.log('Test 100 random numbers')
check_serialization(test3);

let test4 = generateArray(1, 500, 300);
console.log('Test 500 random numbers')
check_serialization(test4);

let test5 = generateArray(1, 1000, 300);
console.log('Test 1000 random numbers')
check_serialization(test5);

let test1 = [1, 8, 4, 9, 7];
console.log('Test all numbers of 1 digit')
check_serialization(test1);

let test7 = generateArray(10, 15, 99);
console.log('Test all numbers of 2 digits')
check_serialization(test7);

let test8 = generateArray(100, 20, 300);
console.log('Test all numbers of 3 digits')
check_serialization(test8);

let test9 = [...test1, ...test8, ...test7]
console.log('Test 300 tripple numbers')
check_serialization(test9);