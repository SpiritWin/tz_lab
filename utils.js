export var SERIALIZE_POS0_TABLE = {};
export var SERIALIZE_POS1_TABLE = {};
export var DESERIALIZE_POS0_TABLE = {};
export var DESERIALIZE_POS1_TABLE = {};
export var SERIALIZE_POS0_TABLE_SIZE = 0;
export var SERIALIZE_POS1_TABLE_SIZE = 0;
export var SERIALIZE_ITEM_DELIMETER = " ";


function create_serialize_pos0_table() {
  var chars_list = concat_lists(rangeChars('A', 'Z'), rangeChars('a', 'z'));
  return create_serialize_table(chars_list);
}

function create_serialize_pos1_table() {
  var chars_list = rangeChars('0', '9');
  return create_serialize_table(chars_list);
}

function create_deserialize_pos0_table() {
  return create_deserialize_table(SERIALIZE_POS0_TABLE);
}

function create_deserialize_pos1_table() {
  return create_deserialize_table(SERIALIZE_POS1_TABLE);
}

function create_deserialize_table(serialize_table) {
  var deserialize_table = {};
  for (var key in serialize_table) {
      var value = serialize_table[key];
      deserialize_table[value] = key;
  }
  return deserialize_table;
}

function rangeChars(fromChar, toChar) {
  var result = [];
  for (var charOrd = fromChar.charCodeAt(0); charOrd <= toChar.charCodeAt(0); charOrd++) {
      result.push(String.fromCharCode(charOrd));
  }
  return result;
}

function concat_lists() {
  var result = [];
  for (var i = 0; i < arguments.length; i++) {
      var curr_list = arguments[i];
      for (var j = 0; j < curr_list.length; j++) {
          result.push(curr_list[j]);
      }
  }
  return result;
}

function create_serialize_table(chars_list) {
  var table = {};
  var index = 0;
  for (var i = 0; i < chars_list.length; i++) {
      var curr_char = chars_list[i];
      table[index] = curr_char;
      index++;
  }
  return table;
}

SERIALIZE_POS0_TABLE = create_serialize_pos0_table();
SERIALIZE_POS1_TABLE = create_serialize_pos1_table();
DESERIALIZE_POS0_TABLE = create_deserialize_pos0_table();
DESERIALIZE_POS1_TABLE = create_deserialize_pos1_table();
SERIALIZE_POS0_TABLE_SIZE = Object.keys(SERIALIZE_POS0_TABLE).length;
SERIALIZE_POS1_TABLE_SIZE = Object.keys(SERIALIZE_POS1_TABLE).length;