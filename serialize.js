import {SERIALIZE_POS0_TABLE_SIZE, SERIALIZE_POS0_TABLE, SERIALIZE_POS1_TABLE, SERIALIZE_ITEM_DELIMETER} from './utils.js'

export function numbers_to_string(numbers) {
    return numbers.join(SERIALIZE_ITEM_DELIMETER);
}

export function serialize(text) {
    return serialize_text(text);
}

function serialize_value(value) {
    var digit0 = value % SERIALIZE_POS0_TABLE_SIZE;
    var digit1 = Math.floor(value / SERIALIZE_POS0_TABLE_SIZE);
    var char0 = SERIALIZE_POS0_TABLE[digit0];
    if (digit1 === 0) {
        return char0;
    }
    return SERIALIZE_POS1_TABLE[digit1] + char0;
}

function serialize_list(values_list) {
    return values_list.map(function(value) {
        return serialize_value(value);
    }).join('');
}

export function serialize_text(text) {
    var values_list = parse_text(text);
    return serialize_list(values_list);
}

function parse_text(text) {
    var values_list = [];
    var trimmed_text = text.trim();
    var splitted_text = trimmed_text.split(SERIALIZE_ITEM_DELIMETER);

    splitted_text.forEach(function(curr_str) {
        if (curr_str.length === 0) {
            return;
        }
        var value = parseInt(curr_str);
        values_list.push(value);
    });

    return values_list;
}

