import {serialize_text, numbers_to_string} from './serialize.js';
import {deserialize_text} from './deserialize.js';

export const generateArray = (min = 1, length, max) => (
    [...new Array(length)]
      .map(() => Math.round(min + Math.random() * ((max - min) + 1)))
  );

//(-100 + Math.random() * ((100 - (-100)) + 1))

export function check_serialization(numbers) {
    let SummaryInTextLength, SummarySerializedTextLength;
    let in_text = numbers_to_string(numbers);
    let serialized_text = serialize_text(in_text);
    let deserialized_text = deserialize_text(serialized_text);

    let in_text_length = in_text.length;
    let serialized_text_length = serialized_text.length;
    SummaryInTextLength += in_text_length;
    SummarySerializedTextLength += serialized_text_length;

    const ratio = (100.0 * serialized_text_length) / in_text_length;

    console.log("\tINPUT STRING: ", in_text);
    console.log("\tOUTPUT STRING: ", serialized_text);
    console.log("\tRATIO: ", ratio.toFixed(1));
    // console.log("\tDESERIALIZED STRING: ", deserialized_text);
    console.log("\tTEST VALID: ", numbers.every((el, i) => el === deserialized_text[i]) + "\n");
}